import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class ShowList extends JFrame {
    final JTable jTable;
    JButton addBtn;
    JButton deleteBtn;
    JScrollPane jScrollPane;

    public ShowList() {
        this.setTitle("ToDo List");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBounds(300, 300, 500, 500);

        Vector<StringBuilder> column = new Vector<>();
        column.add(new StringBuilder("Title"));
        column.add(new StringBuilder("Description"));
        column.add(new StringBuilder("Date"));

        jTable = new JTable(ItemDataModel.getDataVector(), column);
        jTable.setCellSelectionEnabled(true);

        ListSelectionModel cellSelectionModel = jTable.getSelectionModel();
        cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int selectedRow = jTable.getSelectedRow();
                int selectedColumns = jTable.getSelectedColumn();
                String selectedData = jTable.getValueAt(selectedRow, selectedColumns).toString();
                ItemDataModel.editData(selectedRow, selectedColumns, selectedData);
            }

        });

        btns();


        jScrollPane = new JScrollPane(jTable);
        this.add(jScrollPane);
        this.add(addBtn);
        this.add(deleteBtn);
        this.setLayout(new FlowLayout());
        this.setVisible(true);
    }

    private void btns() {
        addBtn = new JButton("Add");
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddItemForm addItemForm = new AddItemForm();
                ShowList.super.dispose();
            }
        });
        deleteBtn = new JButton("Delete");
        deleteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ItemDataModel.removeItem(jTable.getSelectedRow());
                ShowList.super.dispose();
                new ShowList();
            }
        });
    }
}
