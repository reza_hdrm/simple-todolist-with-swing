import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddItemForm extends JFrame {

    private JLabel titleLabel;
    private JTextField titleEdit;
    private JLabel descriptionLabel;
    private JTextField descriptionEdit;
    private JLabel dateLabel;
    private JTextField dateEdit;
    private JButton btnAdd;
    private ItemDataModel itemDataModel;

    public AddItemForm() {
        this.setTitle("Add Item Form");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        initComponent();
        setComponentBounds();
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ItemDataModel.setData(titleEdit.getText(), descriptionEdit.getText(), dateEdit.getText());
                AddItemForm.super.dispose();
                new ShowList();
            }
        });
        jFrameAdds();
        this.setBounds(300, 400, 350, 250);
        this.setLayout(null);
        this.setVisible(true);
    }

    private void setComponentBounds() {
        titleLabel.setBounds(15, 5, 40, 30);
        titleEdit.setBounds(50, 5, 80, 30);
        descriptionLabel.setBounds(15, 40, 90, 30);
        descriptionEdit.setBounds(100, 40, 80, 30);
        dateLabel.setBounds(15, 75, 40, 30);
        dateEdit.setBounds(55, 75, 80, 30);
        btnAdd.setBounds(135, 180, 80, 30);
    }

    private void jFrameAdds() {
        this.add(titleLabel);
        this.add(titleEdit);
        this.add(descriptionLabel);
        this.add(descriptionEdit);
        this.add(dateLabel);
        this.add(dateEdit);
        this.add(btnAdd);
    }

    private void initComponent() {
        titleEdit = new JTextField();
        titleLabel = new JLabel("Title");
        descriptionEdit = new JTextField();
        descriptionLabel = new JLabel("Description");
        dateEdit = new JTextField();
        dateLabel = new JLabel("Date");
        btnAdd = new JButton("Add");
    }
}
