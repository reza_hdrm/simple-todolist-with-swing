
public class Main {
    ShowList showList;

    public Main() {
        ItemDataModel.init();
        showList = new ShowList();
    }

    public static void main(String[] args) {
        new Main();
    }
}
