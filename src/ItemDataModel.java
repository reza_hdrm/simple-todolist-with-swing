import java.util.Vector;

public class ItemDataModel {
    private static Vector<Vector<String>> data;

    private ItemDataModel() {
        data = new Vector<>();
    }

    public static void init() {
         new ItemDataModel();
    }

    public static void setData(String title, String description, String date) {
        Vector<String> v = new Vector<>();
        v.add(title);
        v.add(description);
        v.add(date);
        data.add(v);
    }

    public static Vector<Vector<String>> getDataVector() {
        return data;
    }

    public static void editData(int row, int column, String str) {
        data.get(row).set(column, str);
    }

    public static void removeItem(int index) {
        data.remove(index);
    }
}
